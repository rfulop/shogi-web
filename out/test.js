"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const shogi_1 = require("./shogi");
const ava_1 = __importDefault(require("ava"));
ava_1.default('Ensure Lance can move forward to empty square', t => {
    let board = new shogi_1.Board();
    t.true(board.isLegalMove([0, 0], [1, 0], 1));
});
ava_1.default('Ensure Lance cannot move forward to occupied square', t => {
    let board = new shogi_1.Board();
    t.true(board.isLegalMove([0, 0], [2, 0], 1));
});
//# sourceMappingURL=test.js.map