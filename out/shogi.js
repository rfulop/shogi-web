"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PieceCode;
(function (PieceCode) {
    PieceCode[PieceCode["Empty"] = 0] = "Empty";
    PieceCode[PieceCode["Lance"] = 1] = "Lance";
    PieceCode[PieceCode["UpgradedLance"] = 2] = "UpgradedLance";
    PieceCode[PieceCode["Knight"] = 3] = "Knight";
    PieceCode[PieceCode["UpgradedKnight"] = 4] = "UpgradedKnight";
    PieceCode[PieceCode["GoldGeneral"] = 5] = "GoldGeneral";
    PieceCode[PieceCode["SilverGeneral"] = 6] = "SilverGeneral";
    PieceCode[PieceCode["UpgradedSilverGeneral"] = 7] = "UpgradedSilverGeneral";
    PieceCode[PieceCode["King"] = 8] = "King";
    PieceCode[PieceCode["Rook"] = 9] = "Rook";
    PieceCode[PieceCode["UpgradedRook"] = 10] = "UpgradedRook";
    PieceCode[PieceCode["Bishop"] = 11] = "Bishop";
    PieceCode[PieceCode["UpgradedBishop"] = 12] = "UpgradedBishop";
    PieceCode[PieceCode["Pawn"] = 13] = "Pawn";
    PieceCode[PieceCode["UpgradedPawn"] = 14] = "UpgradedPawn";
    PieceCode[PieceCode["Lance2"] = 15] = "Lance2";
    PieceCode[PieceCode["UpgradedLance2"] = 16] = "UpgradedLance2";
    PieceCode[PieceCode["Knight2"] = 17] = "Knight2";
    PieceCode[PieceCode["UpgradedKnight2"] = 18] = "UpgradedKnight2";
    PieceCode[PieceCode["GoldGeneral2"] = 19] = "GoldGeneral2";
    PieceCode[PieceCode["SilverGeneral2"] = 20] = "SilverGeneral2";
    PieceCode[PieceCode["UpgradedSilverGeneral2"] = 21] = "UpgradedSilverGeneral2";
    PieceCode[PieceCode["King2"] = 22] = "King2";
    PieceCode[PieceCode["Rook2"] = 23] = "Rook2";
    PieceCode[PieceCode["UpgradedRook2"] = 24] = "UpgradedRook2";
    PieceCode[PieceCode["Bishop2"] = 25] = "Bishop2";
    PieceCode[PieceCode["UpgradedBishop2"] = 26] = "UpgradedBishop2";
    PieceCode[PieceCode["Pawn2"] = 27] = "Pawn2";
    PieceCode[PieceCode["UpgradedPawn2"] = 28] = "UpgradedPawn2";
})(PieceCode || (PieceCode = {}));
function sign(n) {
    return n / Math.abs(n);
}
function playerOf(piece) {
    if (piece === PieceCode.Empty)
        return null;
    else if (piece >= 15)
        return 2;
    else
        return 1;
}
class Board {
    constructor() {
        //idx 0 = 9i, idx 8 = 1i
        this.board = [PieceCode.Lance, PieceCode.Knight, PieceCode.GoldGeneral, PieceCode.SilverGeneral, PieceCode.King, PieceCode.SilverGeneral, PieceCode.GoldGeneral, PieceCode.Knight, PieceCode.Lance,
            PieceCode.Empty, PieceCode.Rook, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Bishop, PieceCode.Empty,
            PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn,
            PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty,
            PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty,
            PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty,
            PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2,
            PieceCode.Empty, PieceCode.Bishop2, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Rook2, PieceCode.Empty,
            PieceCode.Lance2, PieceCode.Knight2, PieceCode.GoldGeneral2, PieceCode.SilverGeneral2, PieceCode.King2, PieceCode.SilverGeneral2, PieceCode.GoldGeneral2, PieceCode.Knight2, PieceCode.Lance2];
    }
    index(rankFile) {
        return rankFile[0] * 9 + rankFile[1];
    }
    move(src, dst) {
        let srcIndex = this.index(src);
        let dstIndex = this.index(dst);
        let piece = this.board[srcIndex];
        let capturedPiece = this.board[dstIndex];
        this.board[dstIndex] = piece;
        if (capturedPiece === null) {
            return capturedPiece;
        }
        else {
            return null;
        }
    }
    forwardForPlayer(player) {
        return (player === 1) ? 1 : -1;
    }
    isLegalGoldGeneral(src, dst, player) {
        let forward = this.forwardForPlayer(player);
        return ((src[0] + forward === dst[0] && [-1, 0, 1].includes(src[1] - dst[1])) ||
            (src[0] === dst[0] && [-1, 1].includes(src[1] - dst[1])) ||
            (src[0] - forward === dst[0] && src[1] === dst[1]));
    }
    isLegalSilverGeneral(src, dst, player) {
        let forward = this.forwardForPlayer(player);
        return ((src[0] + forward === dst[0] && [-1, 0, 1].includes(src[1] - dst[1])) ||
            (src[0] - forward === dst[0] && [-1, 1].includes(src[1] - dst[1])));
    }
    isLegalKing(src, dst) {
        return ([-1, 0, 1].includes(dst[0] - src[0]) && [-1, 0, 1].includes(dst[1] - src[1]));
    }
    isLegalKnight(src, dst, player) {
        let forward = this.forwardForPlayer(player);
        return (src[0] + forward * 2 === dst[0] && [-1, 1].includes(src[1] - dst[1]));
    }
    isLegalPawn(src, dst, player) {
        let forward = this.forwardForPlayer(player);
        return (src[0] + forward === dst[0] && src[1] === dst[1]);
    }
    isLegalLance(src, dst, player) {
        let forward = this.forwardForPlayer(player);
        if (src[1] != dst[1]) {
            return false;
        }
        console.log('begin loop');
        //make sure all intermediate spaces are all empty
        let loc = [-1, src[1]];
        for (loc[0] = src[0] + forward; loc[0] != dst[0]; loc[0] += forward) {
            console.log('loc: ' + loc);
            if (this.board[this.index(loc)] != PieceCode.Empty) {
                return false;
            }
        }
        return true;
    }
    isLegalRook(src, dst) {
        if (src[0] != dst[0] && src[1] != dst[1]) {
            return false;
        }
        else if (src[0] == dst[0]) {
            let forward = sign(dst[0] - src[0]);
            let loc = [-1, src[1]];
            for (loc[0] = src[0] + forward; loc[0] != dst[0]; loc[0] += forward) {
                if (this.board[this.index(loc)] != PieceCode.Empty) {
                    return false;
                }
            }
            return true;
        }
        else {
            let forward = sign(dst[1] - src[1]);
            let loc = [src[0], -1];
            for (loc[1] = src[1] + forward; loc[1] != dst[1]; loc[1] += forward) {
                if (this.board[this.index(loc)] != PieceCode.Empty) {
                    return false;
                }
            }
            return true;
        }
    }
    isLegalBishop(src, dst) {
        return false;
    }
    isLegalMove(src, dst, player) {
        let srcIndex = this.index(src);
        let dstIndex = this.index(dst);
        let piece = this.board[srcIndex];
        console.log('piece: ' + piece);
        let destinationPiece = this.board[dstIndex];
        if (playerOf(piece) !== player) {
            console.log('failure on player\'s piece');
            return false;
        }
        else if (playerOf(destinationPiece) === player) {
            console.log('failure on dest piece');
            return false;
        }
        else if (srcIndex === dstIndex) {
            console.log('src not dst');
            return false;
        }
        //@todo: check if move puts player into check
        switch (piece) {
            case PieceCode.UpgradedPawn:
            case PieceCode.UpgradedPawn2:
            case PieceCode.UpgradedKnight:
            case PieceCode.UpgradedKnight2:
            case PieceCode.UpgradedLance:
            case PieceCode.UpgradedLance2:
            case PieceCode.GoldGeneral:
            case PieceCode.GoldGeneral2:
                return this.isLegalGoldGeneral(src, dst, player);
            case PieceCode.SilverGeneral:
            case PieceCode.SilverGeneral2:
                return this.isLegalSilverGeneral(src, dst, player);
            case PieceCode.King:
            case PieceCode.King2:
                return this.isLegalKing(src, dst);
            case PieceCode.Knight:
            case PieceCode.Knight2:
                return this.isLegalKnight(src, dst, player);
            case PieceCode.Lance:
            case PieceCode.Lance2:
                return this.isLegalLance(src, dst, player);
            case PieceCode.Pawn:
            case PieceCode.Pawn2:
                return this.isLegalPawn(src, dst, player);
            case PieceCode.UpgradedRook:
            case PieceCode.UpgradedRook2:
                return this.isLegalKing(src, dst) || this.isLegalRook(src, dst);
            case PieceCode.Rook:
            case PieceCode.Rook2:
                return this.isLegalRook(src, dst);
            case PieceCode.UpgradedBishop:
            case PieceCode.UpgradedBishop2:
                return this.isLegalKing(src, dst) || this.isLegalBishop(src, dst);
            case PieceCode.Bishop:
            case PieceCode.Bishop2:
                return this.isLegalBishop(src, dst);
        }
        return false;
    }
    drop(piece, dst) {
        let idx = this.index(dst);
        this.board[idx] = piece;
    }
}
exports.Board = Board;
//# sourceMappingURL=shogi.js.map