import { Board } from './shogi'
import test from 'ava'

test('Ensure Lance can move forward to empty square', t => {
    let board = new Board()
    t.true(board.isLegalMove([0, 0], [1, 0], 1))
})

test('Ensure Lance cannot move forward to occupied square', t => {
    let board = new Board()
    t.false(board.isLegalMove([0, 0], [2, 0], 1))
})

