enum PieceCode {
    Empty = 0,
    Lance,
    UpgradedLance,
    Knight,
    UpgradedKnight,
    GoldGeneral,
    SilverGeneral,
    UpgradedSilverGeneral,
    King,
    Rook,
    UpgradedRook,
    Bishop,
    UpgradedBishop,
    Pawn,
    UpgradedPawn,
    Lance2,
    UpgradedLance2,
    Knight2,
    UpgradedKnight2,
    GoldGeneral2,
    SilverGeneral2,
    UpgradedSilverGeneral2,
    King2,
    Rook2,
    UpgradedRook2,
    Bishop2,
    UpgradedBishop2,
    Pawn2,
    UpgradedPawn2
}

function sign(n: number) {
    return n / Math.abs(n)
}

function playerOf(piece: PieceCode) {
    if (piece === PieceCode.Empty)
        return null
    else if (piece >= 15)
        return 2
    else
        return 1
}

export class Board {
    board: Array<number>
    constructor() {
        //idx 0 = 9i, idx 8 = 1i
        this.board = [PieceCode.Lance, PieceCode.Knight, PieceCode.GoldGeneral, PieceCode.SilverGeneral, PieceCode.King, PieceCode.SilverGeneral, PieceCode.GoldGeneral, PieceCode.Knight, PieceCode.Lance,
        PieceCode.Empty, PieceCode.Rook, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Bishop, PieceCode.Empty,
        PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn, PieceCode.Pawn,
        PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty,
        PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty,
        PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty,
        PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2, PieceCode.Pawn2,
        PieceCode.Empty, PieceCode.Bishop2, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Empty, PieceCode.Rook2, PieceCode.Empty,
        PieceCode.Lance2, PieceCode.Knight2, PieceCode.GoldGeneral2, PieceCode.SilverGeneral2, PieceCode.King2, PieceCode.SilverGeneral2, PieceCode.GoldGeneral2, PieceCode.Knight2, PieceCode.Lance2];
    }

    index(rankFile: Array<number>) {
        return rankFile[0] * 9 + rankFile[1]
    }

    move(src: Array<number>, dst: Array<number>) {
        let srcIndex = this.index(src)
        let dstIndex = this.index(dst)
        let piece = this.board[srcIndex]
        let capturedPiece = this.board[dstIndex]
        this.board[dstIndex] = piece
        if (capturedPiece === null) {
            return capturedPiece
        }
        else {
            return null
        }
    }

    forwardForPlayer(player: number): number {
        return (player === 1) ? 1 : -1;
    }

    isLegalGoldGeneral(src: Array<number>, dst: Array<number>, player: number): boolean {
        let forward = this.forwardForPlayer(player);
        return ((src[0] + forward === dst[0] && [-1, 0, 1].includes(src[1] - dst[1])) ||
            (src[0] === dst[0] && [-1, 1].includes(src[1] - dst[1])) ||
            (src[0] - forward === dst[0] && src[1] === dst[1]));
    }

    isLegalSilverGeneral(src: Array<number>, dst: Array<number>, player: number): boolean {
        let forward = this.forwardForPlayer(player);
        return ((src[0] + forward === dst[0] && [-1, 0, 1].includes(src[1] - dst[1])) ||
            (src[0] - forward === dst[0] && [-1, 1].includes(src[1] - dst[1])));
    }

    isLegalKing(src: Array<number>, dst: Array<number>): boolean {
        return ([-1, 0, 1].includes(dst[0] - src[0]) && [-1, 0, 1].includes(dst[1] - src[1]))
    }

    isLegalKnight(src: Array<number>, dst: Array<number>, player: number): boolean {
        let forward = this.forwardForPlayer(player)
        return (src[0] + forward * 2 === dst[0] && [-1, 1].includes(src[1] - dst[1]));
    }

    isLegalPawn(src: Array<number>, dst: Array<number>, player: number): boolean {
        let forward = this.forwardForPlayer(player)
        return (src[0] + forward === dst[0] && src[1] === dst[1])
    }

    isLegalLance(src: Array<number>, dst: Array<number>, player: number): boolean {
        let forward = this.forwardForPlayer(player)
        if (src[1] != dst[1]) {
            return false
        }
        console.log('begin loop')
        //make sure all intermediate spaces are all empty
        let loc = [-1, src[1]]
        for (loc[0] = src[0] + forward; loc[0] != dst[0]; loc[0] += forward) {
            console.log('loc: ' + loc)
            if (this.board[this.index(loc)] != PieceCode.Empty) {
                return false
            }
        }
        return true
    }

    isLegalRook(src: Array<number>, dst: Array<number>): boolean {
        if (src[0] != dst[0] && src[1] != dst[1]) {
            return false
        }
        else if (src[0] == dst[0]) {
            let forward = sign(dst[0] - src[0])
            let loc = [-1, src[1]]
            for (loc[0] = src[0] + forward; loc[0] != dst[0]; loc[0] += forward) {
                if (this.board[this.index(loc)] != PieceCode.Empty) {
                    return false
                }
            }
            return true
        }
        else {
            let forward = sign(dst[1] - src[1])
            let loc = [src[0], -1]
            for (loc[1] = src[1] + forward; loc[1] != dst[1]; loc[1] += forward) {
                if (this.board[this.index(loc)] != PieceCode.Empty) {
                    return false
                }
            }
            return true
        }
    }

    isLegalBishop(src: Array<number>, dst: Array<number>): boolean {
        return false
    }

    isLegalMove(src: Array<number>, dst: Array<number>, player: number): boolean {
        let srcIndex = this.index(src)
        let dstIndex = this.index(dst)
        let piece = this.board[srcIndex]
        console.log('piece: ' + piece)
        let destinationPiece = this.board[dstIndex]
        if (playerOf(piece) !== player) {
            console.log('failure on player\'s piece')
            return false
        }
        else if (playerOf(destinationPiece) === player) {
            console.log('failure on dest piece')
            return false
        }
        else if (srcIndex === dstIndex) {
            console.log('src not dst')
            return false
        }
        //@todo: check if move puts player into check

        switch (piece) {
            case PieceCode.UpgradedPawn:
            case PieceCode.UpgradedPawn2:
            case PieceCode.UpgradedKnight:
            case PieceCode.UpgradedKnight2:
            case PieceCode.UpgradedLance:
            case PieceCode.UpgradedLance2:
            case PieceCode.GoldGeneral:
            case PieceCode.GoldGeneral2:
                return this.isLegalGoldGeneral(src, dst, player)
            case PieceCode.SilverGeneral:
            case PieceCode.SilverGeneral2:
                return this.isLegalSilverGeneral(src, dst, player)
            case PieceCode.King:
            case PieceCode.King2:
                return this.isLegalKing(src, dst)
            case PieceCode.Knight:
            case PieceCode.Knight2:
                return this.isLegalKnight(src, dst, player)
            case PieceCode.Lance:
            case PieceCode.Lance2:
                return this.isLegalLance(src, dst, player)
            case PieceCode.Pawn:
            case PieceCode.Pawn2:
                return this.isLegalPawn(src, dst, player)
            case PieceCode.UpgradedRook:
            case PieceCode.UpgradedRook2:
                return this.isLegalKing(src, dst) || this.isLegalRook(src, dst)
            case PieceCode.Rook:
            case PieceCode.Rook2:
                return this.isLegalRook(src, dst)
            case PieceCode.UpgradedBishop:
            case PieceCode.UpgradedBishop2:
                return this.isLegalKing(src, dst) || this.isLegalBishop(src, dst)
            case PieceCode.Bishop:
            case PieceCode.Bishop2:
                return this.isLegalBishop(src, dst)
        }

        return false
    }

    drop(piece: PieceCode, dst: Array<number>) {
        let idx = this.index(dst)
        this.board[idx] = piece
    }
}
